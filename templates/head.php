<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">


  <?php wp_head(); ?>

	<!-- COMMON TAGS -->
	<meta charset="utf-8">
	<title>The Avalon</title>
	<!-- Search Engine -->
	<meta name="description" content="Life in a Masterpiece">
	<meta name="image" content="http://mustard.studio/avalonweb/wp-content/uploads/2018/06/download_bg.png">
	<!-- Schema.org for Google -->
	<meta itemprop="name" content="The Avalon">
	<meta itemprop="description" content="Life in a Masterpiece">
	<meta itemprop="image" content="http://mustard.studio/avalonweb/wp-content/uploads/2018/06/download_bg.png">
	<!-- Open Graph general (Facebook, Pinterest & Google+) -->
	<meta name="og:title" content="The Avalon">
	<meta name="og:description" content="Life in a Masterpiece">
	<meta name="og:image" content="http://mustard.studio/avalonweb/wp-content/uploads/2018/06/download_bg.png">
	<meta name="og:url" content="http://theavalon.co.za/">
	<meta name="og:site_name" content="The Avalon">
	<meta name="og:type" content="website">

</head>
