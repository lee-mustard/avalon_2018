<style>


@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px)
and (orientation : landscape) {

	.spacer-right-single{
		padding:50px;
	}

	.bg-right {
	    min-height: 70vh;
	}


}


@media only screen and (max-width: 1280px) {
	.overflow-border-tax {
		padding-top:1rem;
		margin-left:-60px;
		display:inline-block;
		padding-right:2rem;
		padding-bottom:0;
	}

	.text-m-lg p {
		font-size:17px;
	}

	h1.negative-space {
    font-size: 2rem;
    margin-bottom: 0px;
    font-weight: 300;
    text-transform: uppercase;
	}

	.selector {
		font-size:17px;
	}
}

@media only screen
and (min-device-width : 320px)
and (max-device-width : 667px) {
	.bg-right {
		min-height:50vh;
	}

	section.apartment-all .row {
		padding:15px;
		padding-left:15px;
	}

	.overflow-border-tax {
		margin-left:-45px;
		padding-right:0;
		padding-top:2rem;
		padding-bottom:0;
		padding-left:20px;
		padding-right:20px;
	}

	.negative-space {
		margin-left:0;
		padding:15px;
	}

	h1.negative-space {
		    font-size: 2rem;
		    text-transform: uppercase;
		    font-weight: 300;
	}

	p.spacer-lg-l {
		font-size:14px;
	}

	.row-heights-m {
	    margin-top: 0.2rem;
	}

}

@media only screen
and (min-device-width : 769px)
and (max-device-width : 1024px)  {

	.bg-right {
	    min-height: 50vh;
	}
}

@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px)
and (orientation : landscape)
 and (-webkit-min-device-pixel-ratio: 1)
  {

	.spacer-right-single{
		padding:50x;
	}

	.bg-right {
	    min-height: 70vh;
	}

	h1.negative-space {
	    font-size: 2rem;
	}

}

@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px)
and (orientation : portrait)
 and (-webkit-min-device-pixel-ratio: 1)
  {

	.spacer-right-single{
		padding:50x;
	}

	.bg-right {
	    min-height: 70vh;
	}

	h1.negative-space {
	    font-size: 2rem;
	}

}


</style>
<section class="single">
	<div class="row  no-m-a">

		<!-- col -->
		<div class="col-md-6 col-xl-6 xl-custom-left col-sm-12 col-xs-12 col-12 h-100 align-self-center spacer-right-single">

			<div class="overflow-border-tax">
				<h1 class="negative-space">
					<?php   // Get terms for post
					 $terms = get_the_terms( $post->ID , 'types' );
					 // Loop over each item since it's an array
					 if ( $terms != null ){
					 foreach( $terms as $term ) {
					 // Print the name method from $term which is an OBJECT
					 print $term->name ;
					 // Get rid of the other data stored in the object, since it's not needed
					 unset($term);
					} } ?> <?php //the_id(); ?>

			   <br /><span>*CLICK ON A UNIT NUMBER TO VIEW THE UNIT PLAN</span></h1><br />
			</div>

	 <?
							$terms = get_the_terms( $post->ID, 'types' );
							if ( !empty( $terms ) ){
							    // get the first term
							    $term = array_shift( $terms );
							    $tax = $term->slug;
							} ?>


			<div class="row row-heights-m">

				<div class="col-md-12 no-gutters spacer-t-m col-12 spacer-lg-l">
					<strong>APARTMENTS</strong>

					<div class="divTable">
						<div class="divTableBody">

							<table id="tablePreview" class="table">
							  <tbody>
								<?php
									$args = array( 'post_type' => 'apartment', 'orderby'   => 'title', 'order' => 'ASC', 'types' => $tax , 'posts_per_page' => -1, );
									$looprates = new WP_Query( $args );
									while ( $looprates->have_posts() ) : $looprates->the_post(); ?>

	  									<?php if( get_post_meta($post->ID, 'is_sold', true) ) { ?>
												<tr>
											       <td class="title_hold"><?php the_title(); ?></strong></td>
											      <td class="sold">SOLD</td>
											      <td></td>
											      <td></td>
											    </tr>

											    <?php } else { ?>

		  										<a href="#">
												   <tr class='clickable-row' data-href='<?php the_permalink(); ?>'>
												      <td class="title_hold"><?php the_title(); ?></strong></td>
												      <td><?php the_field('price'); ?></td>
												      <td><?php the_field('square_meters');?>m<sup>2</sup></td>
												      <td><?php the_field('type_description'); ?></td>
												    </tr>
		  										</a>
											<?php } ?>
								<?php endwhile; wp_reset_query();?>

							  </tbody>
							</table>

						</div>
					</div>
				</div>

			</div>

		</div>
		<!-- col -->

		<!-- col -->
		<div class="col-md-6 xl-custom-right col-xl-6 col-sm-12 col-xs-12 col-12 h-100 no-m-a-right ">

				<?php if( get_post_meta($post->ID, 'floor_plan', true) ) { ?>
						<img class="img-fluid" src="<?php the_field('floor_plan'); ?>" />
					<?php } else { ?>
						<img class="img-fluid" src="http://mustard.studio/avalonweb/wp-content/uploads/2018/08/test-map_1.png" />
				<?php } ?>




				<div class="row row-heights-m">


					<div class="col-md-3 align-self-center no-gutters spacer-t-m col-6 ">
						<div id="textbox">


							<p class="alignleft">

								<strong class="dark">UNIT: </strong><br />

								<?php if( get_post_meta($post->ID, 'garden', true) ) { ?>
										<strong class="dark">GARDEN: </strong><br />
										<?php } else { ?>
								<?php } ?>

								<?php if( get_post_meta($post->ID, 'balcony', true) ) { ?>
										<strong class="dark">BALCONY: </strong><br />
										<?php } else { ?>
								<?php } ?>


								<strong class="dark">TOTAL: </strong><br />
							</p>
							<p class="alignright">
								<?php the_field('total_size'); ?><sup>2</sup> <br />

								<?php if( get_post_meta($post->ID, 'garden', true) ) { ?>
										<?php the_field('garden'); ?><sup>2</sup> <br />
									<?php } else { ?>
								<?php } ?>
								<?php if( get_post_meta($post->ID, 'balcony', true) ) { ?>
											<?php the_field('balcony'); ?><sup>2</sup> <br />
										<?php } else { ?>
								<?php } ?>

								<?php the_field('total_size'); ?><sup>2</sup> <br />

							</p>


						</div>
						<div style="clear: both;"></div>
					</div>

					<div class="col-md-5  border-dashed  align-self-center no-gutters spacer-t-m col-6 ">
						<img class="img-fluid mx-auto d-block" src="<?php bloginfo('template_directory'); ?>/assets/images/scale.png" />
					</div>

					<div class="col-md-4 no-gutters  align-self-center spacer-t-m col-6 ">
						<a data-toggle="modal" data-target="#exampleModal" target="_blank" class="btn btn-default">Enquire Now </a>
					</div>


				</div>

		</div>
		<!-- col -->

	</div>

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">

	      <div class="modal-header">
	        <h4 class="modal-title">Enquire</h4>
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>

	      <div class="modal-body">

			<div class="form-hold">
					<?php gravity_form(4, false, false, false, '', true, 12); ?>
			</div>

	      </div>
	    </div>
	  </div>
	</div>

</section>


