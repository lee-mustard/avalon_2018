<section class="contact">
		<div class="row no-m">

			<div class="col-md-12 co col-xl-6 col-sm-12 col-xs-12 col-12 h-100 align-self-center contact-hold spacer-right">
				<h1 class="page-title">CONTACT US</h1><br />
				<div class="row no-gutters row-heights-m">

					<div class="col-md-4 spacer-t-m col-12">
						<strong class="dark">Craig Getz</strong><br />
						+27 83 518 2129
					</div>

					<div class="col-md-4 spacer-t-m col-12">
						<strong class="dark">Julian Reynolds</strong><br />
						+27 79 883 7332
					</div>

					<div class="col-md-4 spacer-t-m col-12">
						<strong class="dark">Email Us</strong><br />
						info@theavalon.co.za
					</div>
				</div>

				<div class="form-hold">
					<p>Fill in your details below and a sales broker will be in touch shortly</p>
					<?php gravity_form(2, false, false, false, '', true, 12); ?>
				</div>

			</div>

			<div class="col-md-12 co col-xl-6 col-sm-12 col-xs-12 col-12 h-100 no-m-right">

				<div class="">
				    <div class="map-responsive">
					   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3310.2975052660668!2d18.41552131583511!3d-33.9334755298511!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1dcc6779913126e5%3A0x11a4d0557bd1fb6d!2s123+Hope+St%2C+Gardens%2C+Cape+Town%2C+8001!5e0!3m2!1sen!2sza!4v1532089720803" width="600" height="" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>

			</div>

		</div>
</section>
