<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/scripts/jquery.map-trifecta.js"></script>



<style>


section.apartment-all {
    padding: 50px 0;
}

@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px)
and (orientation : landscape) {

	.spacer-right-single{
		padding:50px;
	}

	.bg-right {
	    min-height: 70vh;
	}


}


@media only screen and (max-width: 1280px) {
	.overflow-border-tax {
		padding-top:1rem;
		margin-left:-60px;
		display:inline-block;
		padding-right:2rem;
		padding-bottom:0;
	}

	.text-m-lg p {
		font-size:17px;
	}

	h1.negative-space {
    font-size: 2rem;
    margin-bottom: 0px;
    font-weight: 300;
    text-transform: uppercase;
	}

	.selector {
		font-size:17px;
	}
}

@media only screen
and (min-device-width : 320px)
and (max-device-width : 667px) {
	.bg-right {
		min-height:50vh;
	}

	section.apartment-all .row {
		padding:15px;
		padding-left:15px;
	}

	.overflow-border-tax {
		margin-left:-45px;
		padding-right:0;
		padding-top:2rem;
		padding-bottom:0;
		padding-left:20px;
		padding-right:20px;
	}

	.negative-space {
		margin-left:0;
		padding:15px;
	}

	h1.negative-space {
		font-size:2rem;
	}

	p.spacer-lg-l {
		font-size:14px;
	}

	.row-heights-m {
	    margin-top: 0.2rem;
	}

}

@media only screen
and (min-device-width : 769px)
and (max-device-width : 1024px)  {

	.bg-right {
	    min-height: 50vh;
	}
}

@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px)
and (orientation : landscape)
 and (-webkit-min-device-pixel-ratio: 1)
  {

	.spacer-right-single{
		padding:50x;
	}

	.bg-right {
	    min-height: 70vh;
	}

	h1.negative-space {
	    font-size: 2rem;
	}

}

@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px)
and (orientation : portrait)
 and (-webkit-min-device-pixel-ratio: 1)
  {

	.spacer-right-single{
		padding:50x;
	}

	.bg-right {
	    min-height: 70vh;
	}

	h1.negative-space {
	    font-size: 2rem;
	}

}


</style>
<section class="apartment-all">
	<div class="row  no-m-a">

		<!-- col -->
		<div class="col-md-6 col-xl-6 xl-custom-left col-sm-12 col-xs-12 col-12 h-100 align-self-center spacer-right-single">

			<div class="overflow-border-tax">
				<h1 class="negative-space"><? $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); echo $term->name ;  ;?>
			   <br /><span>*CLICK ON A UNIT NUMBER TO VIEW THE UNIT PLAN</span></h1><br />
			</div>

			<div class="row row-heights-m">
				<div class="col-md-12 no-gutters spacer-t-m col-12 spacer-lg-l">
					<strong>APARTMENTS</strong>

					<div class="divTable">
						<div class="divTableBody">




						<table id="tablePreview" class="table">
						  <tbody>
								<?php
									$args = array( 'post_type' => 'apartment', 'orderby'   => 'title', 'order' => 'ASC', 'types' => $term->name , 'posts_per_page' => -1, );
									$looprates = new WP_Query( $args );
									while ( $looprates->have_posts() ) : $looprates->the_post(); ?>

  									<?php if( get_post_meta($post->ID, 'is_sold', true) ) { ?>
											<tr>
										      <td class="title_hold"><?php the_title(); ?></strong></td>
										      <td class="sold">SOLD</td>
										      <td></td>
										      <td></td>
										    </tr>

										    <?php } else { ?>

	  										<a id="squidheadlink" href="#">
											   <tr data-mapid="<?php the_slug(); ?>" class="clickable-row" data-href='<?php the_permalink(); ?>'>
											      <td class="title_hold"><?php the_title(); ?></strong></td>
											      <td>R2.389m</td>
											      <td>88m2</td>
											      <td>Studio</td>
											    </tr>
	  										</a>

										<?php } ?>
								<?php endwhile; wp_reset_query();?>


						  </tbody>
						</table>

						</div>
					</div>


					<!-- Page Navi -->
					 <?php if (pa_in_taxonomy('types', 'garden-floor')) { ;?>
						<div class="pagenavi-hold"> <span class="prev-nav" style="opacity: 0.6;"> < PREVIOUS FLOOR</span> |  <span class="prev-nav"><a href="<?php bloginfo('url'); ?>/types/ground-floor/">NEXT FLOOR > </a></span></div>
				            <?php } else { ;?>
					<?php } ?>

					 <?php if (pa_in_taxonomy('types', 'ground-floor')) { ;?>
						<div class="pagenavi-hold"> <span class="prev-nav"> <a href="<?php bloginfo('url'); ?>/types/garden-floor/"> < PREVIOUS FLOOR </span> </a>  |  <span class="prev-nav"><a href="<?php bloginfo('url'); ?>/types/first-floor/">NEXT FLOOR > </a></span></div>
				            <?php } else { ;?>
					<?php } ?>

					 <?php if (pa_in_taxonomy('types', 'first-floor')) { ;?>
						<div class="pagenavi-hold"> <span class="prev-nav"> <a href="<?php bloginfo('url'); ?>/types/ground-floor/"> < PREVIOUS FLOOR</span> </a>  |  <span class="prev-nav"><a href="<?php bloginfo('url'); ?>/types/second-floor/">NEXT FLOOR > </a></span></div>
				            <?php } else { ;?>
					<?php } ?>

					 <?php if (pa_in_taxonomy('types', 'second-floor')) { ;?>
						<div class="pagenavi-hold"> <span class="prev-nav"> <a href="<?php bloginfo('url'); ?>/types/first-floor/"> < PREVIOUS FLOOR</span> </a>  |  <span class="prev-nav"><a href="<?php bloginfo('url'); ?>/types/third-floor/">NEXT FLOOR > </a></span></div>
				            <?php } else { ;?>
					<?php } ?>

					 <?php if (pa_in_taxonomy('types', 'third-floor')) { ;?>
						<div class="pagenavi-hold"> <span class="prev-nav"> <a href="<?php bloginfo('url'); ?>/types/second-floor/"> < PREVIOUS FLOOR</span> </a>  |  <span class="prev-nav"><a href="<?php bloginfo('url'); ?>/types/fourth-floor/">NEXT FLOOR > </a></span></div>
				            <?php } else { ;?>
					<?php } ?>

					 <?php if (pa_in_taxonomy('types', 'fourth-floor')) { ;?>
						<div class="pagenavi-hold"> <span class="prev-nav"> <a href="<?php bloginfo('url'); ?>/types/ground-third/"> < PREVIOUS FLOOR</span> </a>  |  <span class="prev-nav"><a href="<?php bloginfo('url'); ?>/types/second-fifth/">NEXT FLOOR > </a></span></div>
				            <?php } else { ;?>
					<?php } ?>


					 <?php if (pa_in_taxonomy('types', 'fifth-floor')) { ;?>
						<div class="pagenavi-hold"> <span class="prev-nav"> <a href="<?php bloginfo('url'); ?>/types/fourth-floor/"> < PREVIOUS FLOOR</span> </a>  |  <span class="prev-nav"><a href="<?php bloginfo('url'); ?>/types/sixth-floor/">NEXT FLOOR > </a></span></div>
				            <?php } else { ;?>
					<?php } ?>

					 <?php if (pa_in_taxonomy('types', 'sixth-floor')) { ;?>
						<div class="pagenavi-hold"> <span class="prev-nav"> <a href="<?php bloginfo('url'); ?>/types/fifth-floor/"> < PREVIOUS FLOOR</span> </a>  |  <span class="prev-nav"><a href="<?php bloginfo('url'); ?>/types/seventh-floor/">NEXT FLOOR > </a></span></div>
				            <?php } else { ;?>
					<?php } ?>

					 <?php if (pa_in_taxonomy('types', 'seventh-floor')) { ;?>
						<div class="pagenavi-hold"> <span class="prev-nav"> <a href="<?php bloginfo('url'); ?>/types/seventh-floor/"> < PREVIOUS FLOOR</span> </a>  |  <span class="prev-nav"><a href="<?php bloginfo('url'); ?>/types/eigth-floor/">NEXT FLOOR > </a></span></div>
				            <?php } else { ;?>
					<?php } ?>

					 <?php if (pa_in_taxonomy('types', 'eighth-floor')) { ;?>
						<div class="pagenavi-hold"> <span class="prev-nav"> <a href="<?php bloginfo('url'); ?>/types/seventh-floor/"> < PREVIOUS FLOOR</span> </a>  |  <span class="prev-nav"  style="opacity: 0.6;">NEXT FLOOR > </span></div>
				            <?php } else { ;?>
					<?php } ?>
					<!-- Page Navi -->


				</div>
			</div>







		</div>
		<!-- col -->

		<!-- col -->
		<div class="col-md-6 xl-custom-right col-xl-6 col-sm-12 col-xs-12 col-12 h-100 no-m-a-right ">

		<!-- Garden Floor -->
		 <?php if (pa_in_taxonomy('types', 'garden-floor')) { ;?>

			<img  class="map" src="<?php bloginfo('template_directory'); ?>/assets/images/floor_plans/garden_floor.png" usemap="#image-map">
			<map name="image-map">
			    <area onclick="window.open('<?php bloginfo('url'); ?>/apartment/a05/', '_self');"  data-mapid="a05" alt="A05" title="A05" href="<?php bloginfo('url'); ?>/apartment/a05/" coords="381,68,432,139" shape="rect">
			    <area onclick="window.open('<?php bloginfo('url'); ?>/apartment/a06/', '_self');"target=""  data-mapid="a06" alt="A06" title="A06" href="<?php bloginfo('url'); ?>/apartment/a06/" coords="433,69,480,140" shape="rect">
			    <area onclick="window.open('<?php bloginfo('url'); ?>/apartment/a07/', '_self');" target=""  data-mapid="a07" alt="A07" title="A07" href="<?php bloginfo('url'); ?>/apartment/a07/" coords="509,69,541,68,572,155,509,156" shape="poly">
			</map>


            <?php } else { ;?>
		<?php } ?>
		<!-- Garden Floor -->


		<!-- Ground Floor -->
		 <?php if (pa_in_taxonomy('types', 'ground-floor')) { ;?>

			<img src="<?php bloginfo('template_directory'); ?>/assets/images/floor_plans/ground_floor.png" usemap="#image-map">
			<map name="image-map">
			    <area data-mapid="b01" target="" alt="B01" title="B01" href="#" coords="99,100,153,163" shape="rect">
			    <area data-mapid="b02" target="" alt="B02" title="B02" href="#" coords="228,104,181,146" shape="rect">
			    <area data-mapid="b03" target="" alt="B03" title="B03" href="#" coords="275,104,231,146" shape="rect">
			    <area data-mapid="b04" target="" alt="B04" title="B04" href="#" coords="325,103,280,147" shape="rect">
			    <area data-mapid="b05" target="" alt="B05" title="B05" href="#" coords="327,102,374,147" shape="rect">
			    <area data-mapid="b08" target="" alt="B08" title="B08" href="#" coords="570,192,539,100,502,102,500,160,517,159,516,193" shape="poly">
			</map>

	            <?php } else { ;?>
		<?php } ?>
		<!-- Ground Floor -->

		<!-- First Floor -->
		 <?php if (pa_in_taxonomy('types', 'first-floor')) { ;?>

				<img class="map" src="<?php bloginfo('template_directory'); ?>/assets/images/floor_plans/first_floor.png" usemap="#image-map">

				<map name="image-map">
				    <area data-mapid="c02" target="" alt="C02" title="C02" href="#" coords="225,142,275,96" shape="rect">
				    <area data-mapid="c03" target="" alt="C03" title="C03" href="#" coords="276,93,326,141" shape="rect">
				    <area data-mapid="c04" target="" alt="C04" title="C04" href="#" coords="328,96,373,140" shape="rect">
				    <area data-mapid="c05" target="" alt="C05" title="C05" href="#" coords="378,95,426,140" shape="rect">
				    <area data-mapid="c06" target="" alt="C06" title="C06" href="#" coords="474,94,428,140" shape="rect">
				    <area data-mapid="c07" target="" alt="C07" title="C07" href="" coords="505,95,545,96,577,187,522,188,522,159,504,158" shape="poly">
				</map>

	            <?php } else { ;?>
		<?php } ?>
		<!-- First Floor -->

		<!-- Second Floor -->
		 <?php if (pa_in_taxonomy('types', 'second-floor')) { ;?>

				<img class="map" src="<?php bloginfo('template_directory'); ?>/assets/images/floor_plans/second_floor.png" usemap="#image-map">

				<map name="image-map">
				    <area data-mapid="d201" target="" alt="201" title="201" href="#" coords="212,228,271,337" shape="rect">
				    <area data-mapid="d202" target="" alt="202" title="202" href="#" coords="276,229,336,337" shape="rect">
				    <area data-mapid="d03" target="" alt="203" title="203" href="#" coords="338,229,395,336" shape="rect">
				    <area data-mapid="d04"  target="" alt="204" title="204" href="#" coords="400,227,460,336" shape="rect">
				</map>
	            <?php } else { ;?>
		<?php } ?>
		<!-- Second Floor -->

		<!-- Third Floor -->
		 <?php if (pa_in_taxonomy('types', 'third-floor')) { ;?>


				<img class="map"  src="<?php bloginfo('template_directory'); ?>/assets/images/floor_plans/third_floor.png" usemap="#image-map">

				<map name="image-map">
				    <area data-mapid="e300" target="" alt="308" title="308" href="#" coords="64,120,212,230" shape="rect">
				    <area data-mapid="e309"  target="" alt="309" title="309" href="#" coords="214,120,288,259,216,260,247,118" shape="rect">
				    <area data-mapid="e310"  target="" alt="310" title="310" href="#" coords="294,120,370,261" shape="rect">
				    <area data-mapid="e311"  target="" alt="311" title="311" href="#" coords="372,119,448,259" shape="rect">
				    <area  data-mapid="e312" target="" alt="312" title="312" href="#" coords="451,120,524,261" shape="rect">
				    <area data-mapid="e313"  target="" alt="313" title="313" href="#" coords="529,119,604,260" shape="rect">
				    <area data-mapid="e314"  target="" alt="314" title="314" href="#" coords="609,121,607,264,697,238,684,135,646,136,640,120" shape="poly">
				    <area data-mapid="e315"  target="" alt="315" title="315" href="#" coords="749,354,757,368,705,395,656,282,744,241,774,325,752,348" shape="poly">
				    <area data-mapid="e316"  target="" alt="316" title="316" href="#" coords="620,301,651,287,703,396,670,409" shape="poly">
				    <area data-mapid="e317"  target="" alt="317" title="317" href="#" coords="615,302,583,315,634,426,667,413" shape="poly">
				    <area  data-mapid="e318"  target="" alt="318" title="318" href="#" coords="550,336,579,319,632,429,598,443,550,336" shape="poly">
				    <area data-mapid="e319"   target="" alt="319" title="319" href="#" coords="515,351,545,337,595,447,562,459" shape="poly">
				    <area  data-mapid="e320"  target="" alt="320" title="320" href="#" coords="477,367,511,353,559,462,529,477" shape="poly">
				    <area  data-mapid="e302"  target="" alt="302" title="302" href="#" coords="192,465,238,447,279,568,237,583" shape="poly">
				    <area  data-mapid="e303"  target="" alt="303" title="303" href="#" coords="171,472,186,455,230,587,178,603,164,563,181,553,169,513" shape="poly">
				    <area  data-mapid="e304"  target="" alt="304" title="304" href="#" coords="160,556,178,604,101,632,70,611,69,592,69,549,85,548,87,497,146,498,168,517,179,549,164,556,125,547" shape="poly">
				    <area  data-mapid="e305"  target="" alt="305" title="305" href="#" coords="83,407,148,409,146,493,103,492,67,492,69,463,84,463" shape="poly">
				    <area target="" alt="e306" title="306" href="#" coords="147,404,149,321,87,320,86,376,70,375,68,405" shape="poly">
				</map>


	            <?php } else { ;?>
		<?php } ?>
		<!-- Second Floor -->


		</div>
		<!-- col -->




	</div>
</section>