<section class="slider demo">
	<div class="container-fluid no-gutters p-lg-0 p-xs-0 p-md-0">
		<div class="single-item" >

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/header-intro.png" />
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/c15-1.jpg" />
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/header-intro.png" />
			</div>


		</div>

	</div>


</section>

		<div class="" style="position: absolute;z-index: 2;width: 60px;margin-left: -25px;left:50%;text-align: center;">
				<a href="#home-intro"><div class="scroll-down"></div></a>
			</div>

<section id="home-intro"  class="d-flex">


	<div class="container intro-excerpt text-center justify-content-center align-self-center">

		<h1>LIFE IN A <span>MASTERPIECE</span></h1>


		<div class="content-hold">

			<p>Nestled between residential suburbia and the Cape Town CBD lies Avalon, a mixed-use retail, commercial & residential development with an emphasis on luxury living in a part of the city that pulses with life. Positioned on the corners of Mill, Hope and Schoonder Streets in Gardens, Avalon accentuates elegance with Table Mountain as its backdrop and an unparalleled panoramic view of the city bowl.</p>

			<br />

			<p>Comprising of 150 apartments over 8 storeys of stylish living space, Avalon is the pinnacle of contemporary living. The residential component has been designed with a wide selection of apartments ranging from tudios to 4-bedroom duplex penthouses. The interiors have been meticulously selected to bring an international standard of class and sophistication. Step outside Avalon and you’re within walking distance of shops, restaurants, cafes, cultural hotspots and shopping centers. This is a location that spoils you for choice and gives you the freedom to create a truly exceptional lifestyle. The new Gem of Gardens has arrived. Welcome to Luxury Urban Living at its finest.</p>

			<br />

			<a target="_blank" href="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/brochure_compressed.pdf" class="btn btn-default">Download Brochure</a>

		</div>

	</div>
</section>

<section id="home-about" class="d-flex">
	<div class="container justify-content-center align-self-center ">

			<div class="content-hold ">

			<h1 class="text-center"><span>UNPARALELLED</span> LIVING</h1>

			<hr class="quater-w" />

			<div class="row p-t-0 spacer-t-l">

				<div class="align-self-center col-md-4 col-12 col">
					<ul class="features">
						<li>Deluxe studios, 1 & 2-bedroom (38 - 169m2), 3-bedroom, duplexes and penthouses (129 - 310m2)</li>
						<li>Upmarket and convenient luxury living</li>
						<li>Outstanding mountain and harbour views</li>
						<li>Nearby shopping centres, restaurants, coffee shops, Company Gardens, gyms, schools…</li>
						<li>Centrally located</li>
					</ul>
				</div>

				<div class="align-self-center col-md-4 col-12 col">
					<ul class="features">
						<li>Resort-style pool deck & rooftop garden</li>
						<li>Trendy selection of bistros and bars in the building</li>
						<li>High-end fittings & finishes</li>
						<li>Augmented water supply</li>
						<li>Underground parking</li>
						<li>Glamorous lobby</li>
					</ul>
				</div>

				<div class="align-self-center col-md-4 col-12 col">
					<ul class="features">
						<li>High-tech concierge and security service</li>
						<li>Fibre optic Internet and DSTV network</li>
						 <li>On-site co-working spaces</li>
						<li>Double-glazed windows</li>
						<li>Air conditioning</li>
						<li>Environmentally efficient central hot water system</li>
					</ul>
				</div>

			</div>

  		</div>
	</div>
</section>

<section id="home-media" class="d-flex">
	<div class="container justify-content-center align-self-center ">


		<div class="row ">
				<iframe  class="d-block mx-auto" width="800" height="415" src="https://www.youtube.com/embed/QVA33KFZhUo?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen align="center"></iframe>
				<h1 class="spacer-t-xl clearfix text-center"><span>DON’T MISS OUT...</span></h1>
				<div class="container">
					<div class="d-block mx-auto form-body">
						<?php gravity_form(3, false, false, false, '', true, 12); ?>
					</div>
				</div>
		</div>

	</div>
</section>