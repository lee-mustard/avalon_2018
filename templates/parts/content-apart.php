<style>
@media only screen and (max-width: 1280px) {
	.text-m-lg p {
		font-size:17px;
	}

	h1.negative-space {
		font-size:3rem;
	}
}

@media only screen
and (min-device-width : 320px)
and (max-device-width : 667px) {
	.bg-right {
		min-height:50vh;
	}

	section.apartment-m .row {
		padding:15px;
		padding-left:15px;
	}

	.overflow-border {
		margin-left:-45px;
		padding-right:0;
		padding-top:2rem;
		padding-bottom:0;
		padding-left:20px;
		padding-right:20px;
	}

	.negative-space {
		margin-left:0;
		padding:15px;
	}

	h1.negative-space {
		font-size:2rem;
	}

	p.spacer-lg-l {
		font-size:14px;
	}

	.row-heights-m {
	    margin-top: 0.2rem;
	}

}

@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px)
 and (-webkit-min-device-pixel-ratio: 1) {

	.bg-right {
	    min-height: 50vh;
	}
}

@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px)
and (orientation : landscape)
 and (-webkit-min-device-pixel-ratio: 1)
  {

	.spacer-right-single{
		padding:50x;
	}

	.bg-right {
	    min-height: 70vh;
	}

	h1.negative-space {
	    font-size: 2rem;
	}

}

@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px)
and (orientation : portrait)
 and (-webkit-min-device-pixel-ratio: 1)
  {

	.spacer-right-single{
		padding:50x;
	}

	.bg-right {
	    min-height: 70vh;
	}

	h1.negative-space {
	    font-size: 2rem;
	}

}
</style>



<section class="apartment-m">
	<div class="row no-m-a">

		<!-- col -->
		<div class="col-md-6 co col-xl-6 col-sm-12 col-xs-12 col-12 h-100 xl-custom-left align-self-center spacer-right-single">

			<div class="overflow-border">
					<h1 class="negative-space">150 APARTMENTS, <br />
					   <span>1 OPPORTUNITY.</span></h1><br />
			</div>

			<div class="no-gutters  row-heights-m">

				<div class="col-md-12 spacer-t-m text-m-lg col-12">
					<p class="spacer-lg-l">Deluxe Studios, 1 & 2-Bedroom - 38 - 169m2 <br />3-Bedroom, Duplexes and Penthouses - 129 - 310m2 </p>
				</div>

				<div class="col-md-12 gold-text text-m-lg spacer-t-m col-12">
					<strong><p class="spacer-lg-l">PRICED FROM R2 050 000</strong> <br />
					(INCLUDING VAT • NO TRANSFER DUTY) <br />
					ESTIMATED COMPLETION – LATE 2020. </p>
				<a href="<?php bloginfo('url'); ?>/apartments/all-apartments/" class="mspacer-lg-l btn btn-default">EXPLORE APARTMENTS</a>

				</div>

			</div>

		</div>
		<!-- col -->

		<!-- col -->
		<div class="col-md-6 co col-xl-6 col-sm-12 col-xs-12 col-12 xl-custom-right h-100 no-m-a-right bg-right">

		</div>
		<!-- col -->

	</div>
</section>
