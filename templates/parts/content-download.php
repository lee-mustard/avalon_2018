<section class="download">
	<div class="container">
		<div class="row min-h">

			<div class="align-self-center">

				<div class="files-hold">

					<h1>DOWNLOADS</h1>

					<hr class="gold-full">

					<a target="_blank" href="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/brochure_compressed.pdf" class="btn btn-default btn-lg">BROCHURE</a>
					<a target="_blank" href="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/avalon-floorplans-28-June.pdf" class="btn btn-default btn-lg">FLOOR PLANS</a>
					<br />
					<a target="_blank" href="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/Avalon_Price_List_27-June-update_Sold-removed1.pdf" class="btn btn-default btn-lg">PRICE LIST</a>
					<a target="_blank" href="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/unit-types2.pdf" class="btn btn-default btn-lg">UNIT PLANS</a>
					<br />
					<a target="_blank" href="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/images.pdf" target="_blank" class="btn btn-default btn-lg">GALLERY</a>

				</div>

			</div>

		</div>
	</div>
</section>

