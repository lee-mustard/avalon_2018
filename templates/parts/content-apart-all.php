<section class="apartment-all">
	<div class="row  no-m-a">

		<!-- col -->
		<div class="col-md-6 col-xl-6 xl-custom-left col-sm-12 col-xs-12 col-12 h-100 align-self-center spacer-right-single">

			<div class="overflow-border custom-negative">
				<h1 class="negative-space">FLOOR PLANS &<br />
			   <span>APARTMENT TYPES</span></h1><br />
			</div>

			<div class="row row-heights-m">
				<div class="col-md-4 no-gutters spacer-t-m col-6 spacer-lg-l">
					<div class="selector"><a href="<?php bloginfo('url'); ?>/types/garden-floor/"> Garden Floor </a></div>
					<div class="selector"><a href="#">Ground Floor </a></div>
					<div class="selector"><a href="#">First Floor </a></div>
					<div class="selector"><a href="#">Second Floor </a></div>
					<div class="selector"><a href="#">Third Floor </a></div>
				</div>


				<div class="col-md-4 no-gutters spacer-t-m col-6 spacer-lg-l">
						<div class="selector"><a href="#">Fourth Floor </a></div>
						<div class="selector"><a href="#">Fifth Floor </a></div>
						<div class="selector"><a href="#">Sixth Floor </a></div>
						<div class="selector"><a href="#">Seventh Floor </a></div>
						<div class="selector"><a href="#">Eighth Floor </a></div>
				</div>
			</div>

		</div>
		<!-- col -->

		<!-- col -->
		<div class="col-md-6 xl-custom-right col-xl-6 col-sm-12 col-xs-12 col-12 h-100 no-m-a-right bg-right">

		</div>
		<!-- col -->

	</div>
</section>