<section class="map">
	<div class="container-fluid no-gutters p-lg-0 p-xs-0 p-md-0">

<style></style><div class='embed-container'>		<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3310.2975052660668!2d18.41552131583511!3d-33.9334755298511!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1dcc6779913126e5%3A0x11a4d0557bd1fb6d!2s123+Hope+St%2C+Gardens%2C+Cape+Town%2C+8001!5e0!3m2!1sen!2sza!4v1532089720803' width='600' height='450' frameborder='0' style='border:0' allowfullscreen></iframe></div>
	</div>
</section>

<section class="location">
	<div class="container">
		<div class="row">

			<div class="col-md-4 col-12 align-self-center border-right-v text-right text-center-m spacer-right-h">
				<h1>YOUR URBAN<br />ADDRESS<br /><span>123 ON HOPE</span></h1>
			</div>

			<div class="col-md-8 col-12 align-self-center spacer-left-h address-holder">

				<p class="m-b-0"> Avalon lies between the charming suburb of Gardens and the electric and trendy East City Precinct. The area has been identified as one of Cape Town’s most up and coming lifestyle locations, featuring luxuries and amenities  esired for modern-day city living. The development spans Hope, Mill and Schoonder Streets and residents of Avalon can step outside in any direction and find themselves within walking distance of a selection of restaurants,  hopping centres, gyms, private hospitals, heritage wonders, theatres and the lively Kloof Street strip. Avalon’s position offers you a lifestyle of convenience and choice - A truly inspired location for your next home. </p>

			</div>

		</div>
	</div>
</section>

<section class="gallery">
	<div class="container-fluid no-gutters p-lg-0 p-xs-0 p-md-0">

		<div class="single-gallery" >

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/slider-imges3.png" />
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/slider-imges4.png" />
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/slider-imges1.png" />
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/slider-imges2.png" />
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/slider-imges3.png" />
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/slider-imges4.png" />
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/slider-imges1.png" />
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/slider-imges2.png" />
			</div>

		</div>

	</div>
</section>
