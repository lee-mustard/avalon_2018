

<style>


</style>

<section class="galleries">
	<div class="container">

		<div class="align-self-center">
			<div class="row min-h">

			   <div class="col-12 align-self-center">
					<div class="text-center">
						<h1> GALLERY </h1>
					</div>
			   </div>

			   <div class="col-12 col-md-4 col-xl-4  col-xs-6 spacer-sm-m align-self-center">
			      <div class="hovereffect">
			        <a><img class="img-responsive" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/Avalon-Web.png" alt=""></a>
			        <div class="overlay">
			         	<div class="col-md-12 title-h">
				            <h1>INTERNAL 3D RENDERINGS</h1>
							<a  data-toggle="modal" data-target="#InteM" class="btn btn-default" href="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/gallery.png" >GALLERY</a>
			         	</div>
			        </div>
			      </div>
			    </div>
			   <div class="col-12 col-md-4 col-xl-4  col-xs-6 spacer-sm-m align-self-center">
			      <div class="hovereffect">
			       <a data-fancybox="gallery"> <img class="img-responsive" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/Avalon-Web.png" alt=""></a>
			        <div class="overlay">
			         	<div class="col-md-12 title-h">
				            <h1>EXTERNAL 3D RENDERINGS</h1>
							<a  data-toggle="modal" data-target="#extM" class="btn btn-default" href="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/gallery.png" >GALLERY</a>
			         	</div>
			        </div>
			      </div>
			    </div>
			   <div class="col-12 col-md-4 col-xl-4  col-xs-6 spacer-sm-m align-self-center">
			      <div class="hovereffect">
			       <a data-fancybox="gallery"> <img class="img-responsive" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/Avalon-Web.png" alt=""></a>
			        <div class="overlay">
			         	<div class="col-md-12 title-h-mid">
				            <h1>DRONE SHOTS</h1>
							<a  data-toggle="modal" data-target="#DroneM" class="btn btn-default" href="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/gallery.png" >GALLERY</a>
			         	</div>
			        </div>
			      </div>
			    </div>

			</div>
		</div>

	</div>
</section>


<!-- Modal -->
<div class="modal fade" id="DroneM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">
		<div class="single-item" >

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/c15.jpg" alt="">
				<!--<div class="caption-slider"> Caption Here </div> -->
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/z1.jpg" alt="">
				<!--<div class="caption-slider"> Caption Here </div> -->
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/z2.jpg" alt="">
				<!--<div class="caption-slider"> Caption Here </div> -->
			</div>

		</div>
      </div>

    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="InteM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">
		<div class="single-item" >

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/d11.jpg" alt="">
				<!--<div class="caption-slider"> Caption Here </div> -->
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/d12.jpg" alt="">
				<!--<div class="caption-slider"> Caption Here </div> -->
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/d13.jpg" alt="">
				<!--<div class="caption-slider"> Caption Here </div> -->
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/d14.jpg" alt="">
				<!--<div class="caption-slider"> Caption Here </div> -->
			</div>

		</div>
      </div>

    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="extM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">
		<div class="single-item" >

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/c12.jpg" alt="">
				<!--<div class="caption-slider"> Caption Here </div> -->
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/c13.jpg" alt="">
				<!--<div class="caption-slider"> Caption Here </div> -->
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/ccc.jpg" alt="">
				<!--<div class="caption-slider"> Caption Here </div> -->
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/c15.jpg" alt="">
				<!--<div class="caption-slider"> Caption Here </div> -->
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/z2.jpg" alt="">
				<!--<div class="caption-slider"> Caption Here </div> -->
			</div>

		</div>
      </div>

    </div>
  </div>
</div>