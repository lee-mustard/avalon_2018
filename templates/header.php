<div class=" headerbar"> </div>
<header class="banner">
	<div class="container hid d-flex h-100">

		<!-- Desktop Menu -->
		<div class="row justify-content-center  hidden-md-down align-self-center">

			<div class="col-4  hidden-md-down menu-left">

		        <?php
			        wp_nav_menu( array(
			            'theme_location'    => 'primary',
			            'depth'             => 2,
			            'container'         => '<li>',
			            'container_class'   => '',
			            'container_id'      => '',
			            'menu_class'        => 'header-menu',
			            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			            'walker'            => new WP_Bootstrap_Navwalker()
					) );
		        ?>

			</div>

			<div class="col-4  hidden-md-down align-self-center">
				<a href="<?php bloginfo('url'); ?>"><img class="brand-logo mx-auto d-block" src="<?php bloginfo('template_directory'); ?>/assets/images/logo.svg" /></a>
			</div>

			<div class="col-4  hidden-md-down menu-right">

		        <?php
			        wp_nav_menu( array(
			            'theme_location'    => 'secondary',
			            'depth'             => 2,
			            'container'         => '<li>',
			            'container_class'   => '',
			            'container_id'      => '',
			            'menu_class'        => 'header-menu',
			            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			            'walker'            => new WP_Bootstrap_Navwalker()
					) );
		        ?>

			</div>

		</div>
		<!-- Desktop Menu -->



		<!-- Mobile Menu -->
		<div class="row justify-content-center align-self-center hidden-lg-up">

			<div class="col-8 col-xs-8 m-logo m-logo ">
				<a href="<?php // bloginfo('url'); ?>"><img class="brand-logo " src="<?php bloginfo('template_directory'); ?>/assets/images/logo.svg" /></a>
			</div>

			<div class="col-4">
				<span class="menu_open " style="cursor:pointer" onclick="openNav()">&#9776;</span>
			</div>

			<div id="myNav" class="overlay">
			  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			  <div class="overlay-content">
				        <?php
			        wp_nav_menu( array(
			            'theme_location'    => 'primary',
			            'depth'             => 2,
			            'container'         => '<li>',
			            'container_class'   => '',
			            'container_id'      => '',
			            'menu_class'        => 'header-menu',
			            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			            'walker'            => new WP_Bootstrap_Navwalker()
					) );
		        ?>
		        <?php
			        wp_nav_menu( array(
			            'theme_location'    => 'secondary',
			            'depth'             => 2,
			            'container'         => '<li>',
			            'container_class'   => '',
			            'container_id'      => '',
			            'menu_class'        => 'header-menu',
			            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			            'walker'            => new WP_Bootstrap_Navwalker()
					) );
		        ?>
			  </div>
			</div>

		</div>
		<!-- Mobile Menu -->


	</div>

</header>