<footer class="content-info">
  <div class="container  d-flex h-100">
  	<div class="row  justify-content-center align-self-center">

		<div class="col-12 col-xl-2 col-lg-2 col-md-2 text-center-m  justify-content-center align-self-center contact-details padd-0">
			<p><strong>CRAIG GETZ</strong><br />
			+27 83 518 2129</p>
			<p><strong>JULIAN REYNOLDS</strong><br />
			+27 79 883 7332</p>
			<p>INFO@THEAVALON.CO.ZA</p>
		</div>

		<div class="col-12 col-md-2 col-lg-2 col-xl-2  justify-content-center align-self-center">
			<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/client-logo-1.png" />
		</div>

		<div class="col-12 col-xl-2 col-lg-2 col-md-2 justify-content-center align-self-center">
			<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/client-logo-2.png" />
		</div>

		<div class="col-12 col-xl-5 col-lg-5  col-md-6 justify-content-center align-self-center text-center-m  spacer-hack" >
			<h3>FOLLOW OUR ONLINE COMMUNITIES <span class="social-photo"><i class="fab fa-facebook-f fa-lg"></i> <i class="fab fa-instagram fa-lg"></i></span></h3>
			<h3>DON’T MISS OUT...</h3>
			<?php gravity_form(3, false, false, false, '', true, 12); ?>
		</div>
 	</div>
  </div>


</footer>
<div class="gold-bar d-flex h-100">
	<div class="container">
		<div class="d-flex">
			  <div class="mr-auto p-2">&copy; Copyright Avalon 2018. Disclaimer.</div>
			  <div class="p-2"> Designed & Developed by <a target="_blank" href="http://mustard.agency">Mustard.</a></div>
		</div>
	</div>
</div>

