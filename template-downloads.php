<?php
/**
 * Template Name: Downloads Page
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/parts/content-download', 'page'); ?>
<?php endwhile; ?>
