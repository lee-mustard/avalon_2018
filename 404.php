<section class="slider demo">
	<div class="container-fluid no-gutters p-lg-0 p-xs-0 p-md-0">
		<div class="single-item" >

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/header-intro.png" />
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/c15-1.jpg" />
			</div>

			<div class="item">
				<img class="img-fluid mx-auto d-block" src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/06/header-intro.png" />
			</div>


		</div>

	</div>


</section>

		<div class="" style="position: absolute;z-index: 2;width: 60px;margin-left: -25px;left:50%;text-align: center;">
				<a href="#home-intro"><div class="scroll-down"></div></a>
			</div>

<section id="home-intro"  class="d-flex">


	<div class="container intro-excerpt text-center justify-content-center align-self-center">

		<h1>LIFE IN A <span>MASTERPIECE</span></h1>


		<div class="content-hold">

			<p>	<div class="alert alert-warning">
				  <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
				</div>
			</p>

			<br />

			<a target="_blank" href="<?php bloginfo('url'); ?>/wp-content/uploads/2018/07/brochure_compressed.pdf" class="btn btn-default">Download Brochure</a>

		</div>

	</div>
</section>

