/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */


	function openNav() {
	    	document.getElementById("myNav").style.height = "100%";
			var element = document.getElementsByTagName( 'body' )[0]; // '0' to assign the first (and only `HTML` tag)
			element.classList.add('open');


		}

	function closeNav() {
	   		document.getElementById("myNav").style.height = "0%";
	   		var element = document.getElementsByTagName( 'body' )[0]; // '0' to assign the first (and only `HTML` tag)
			element.classList.remove('open');		}



(function($) {

	$(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });

	$('.single-item').slick({
	    autoplay:true,
        dots: false,
	    arrows: true,
	    fade: false,
	    infinite: true,
	    touchThreshold: 100,
	    draggable:true,
		nextArrow: '<div class="arrow_next"></div>',
        prevArrow: '<div class="arrow_prev"></div>',
	});


	$('#DroneM').on('hidden.bs.modal', function (e) {
	$('.single-gallery').slick({
		 slidesToShow: 5,
		  slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 2000,
	    arrows: true,
		nextArrow: '<div class="arrow_next_gal"></div>',
        prevArrow: '<div class="arrow_prev_gal"></div>',

        responsive: [
		    {
		      breakpoint: 480,
		       settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			    }
		    }
		  ]

	});
	})

	$('.single-gallery').slick({
		 slidesToShow: 5,
		  slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 2000,
	    arrows: true,
		nextArrow: '<div class="arrow_next_gal"></div>',
        prevArrow: '<div class="arrow_prev_gal"></div>',

        responsive: [
		    {
		      breakpoint: 480,
		       settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			    }
		    }
		  ]

	});


	$("img[usemap]").mapTrifecta();

/*
    $('img[usemap]').rwdImageMaps();
	$('.map').maphilight();
        $('#squidheadlink').mouseover(function(e) {
            $('#squidhead').mouseover();
        }).mouseout(function(e) {
            $('#squidhead').mouseout();
    }).click(function(e) { e.preventDefault(); });
*/



	// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });

	var map;
	function initMap() {
	  map = new google.maps.Map(document.getElementById('map'), {
	    center: {
	      lat: -34.397,
	      lng: 150.644
	    },
	    zoom: 8
	  });
	}

	$(function() {
	  $('a[href*=#]').on('click', function(e) {
	    e.preventDefault();
	    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
	  });
	});

	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
	    event.preventDefault();
	    $(this).ekkoLightbox();
	});

	$('.modal').on('shown.bs.modal', function (e) {
	    $('.single-item').resize();
	})

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
