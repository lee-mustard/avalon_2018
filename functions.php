<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];


require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'Avalon' ),
) );

register_nav_menus( array(
	'secondary' => __( 'Secondary Menu', 'Avalon' ),
) );


foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


function remove_menus(){
  remove_menu_page( 'index.php' );                  //Dashboard
  remove_menu_page( 'edit.php' );                   //Posts
  remove_menu_page( 'edit-comments.php' );			//Comments
  remove_menu_page( 'tools.php' );                  //Tools

}
add_action( 'admin_menu', 'remove_menus' );

add_filter('admin_footer_text', 'wp_bootstrap_custom_admin_footer');
function wp_bootstrap_custom_admin_footer() {
	echo '<span id="footer-thankyou">Developed by <a style="text-decoration:none;" href="http://mustard.agency" target="_blank">Mustard Agency</a></span>';
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function set_the_terms_in_order ( $terms, $id, $taxonomy ) {
    $terms = wp_cache_get( $id, "{$taxonomy}_relationships_sorted" );
    if ( false === $terms ) {
        $terms = wp_get_object_terms( $id, $taxonomy, array( 'orderby' => 'term_order' ) );
        wp_cache_add($id, $terms, $taxonomy . '_relationships_sorted');
    }
    return $terms;
}
add_filter( 'get_the_terms', 'set_the_terms_in_order' , 10, 4 );

function do_the_terms_in_order () {
    global $wp_taxonomies;  //fixed missing semicolon
    // the following relates to tags, but you can add more lines like this for any taxonomy
    $wp_taxonomies['post_tag']->sort = true;
    $wp_taxonomies['post_tag']->args = array( 'orderby' => 'term_order' );
}
add_action( 'init', 'do_the_terms_in_order');


function the_slug($echo=true){
  $slug = basename(get_permalink());
  do_action('before_slug', $slug);
  $slug = apply_filters('slug_filter', $slug);
  if( $echo ) echo $slug;
  do_action('after_slug', $slug);
  return $slug;
}


function adjacent_post_by_category( $category, $direction, $content ) {

    // Info
    $postIDs = array();
    $currentPost = get_the_ID();


    // Get posts based on category
    $postArray = get_posts( array(

        'category' => $category,
        'orderby'=>'post_date',
        'order'=> 'DESC'

    ) );


    // Get post IDs
    foreach ( $postArray as $thepost ):

        $postIDs[] = $thepost->ID;

    endforeach;


    // Get prev and next post ID
    $currentIndex = array_search( $currentPost, $postIDs );
    $prevID = $postIDs[ $currentIndex - 1 ];
    $nextID = $postIDs[ $currentIndex + 1 ];


    // Return information
    if( $direction == 'next' AND $nextID ):

        return '<a rel="next" href="' . get_permalink( $nextID ) . '">' . $content . '</a>';

    elseif( $direction == 'prev' AND $prevID ):

        return '<a rel="prev" href="' . get_permalink( $prevID ) . '">' . $content . '</a>';

    else:

        return false;

    endif;

}


/**
* Conditional function to check if post belongs to term in a custom taxonomy.
*
* @param    tax      string              taxonomy to which the term belons
* @param    term     int|string|array    attributes of shortcode
* @param    _post    int                 post id to be checked
* @return            BOOL                True if term is matched, false otherwise
*/

function pa_in_taxonomy($tax, $term, $_post = NULL) {
    // if neither tax nor term are specified, return false
    if ( !$tax || !$term ) { return FALSE; }
    // if post parameter is given, get it, otherwise use $GLOBALS to get post
    if ( $_post ) {
        $_post = get_post( $_post );
    } else {
        $_post =& $GLOBALS['post'];
    }
    // if no post return false
    if ( !$_post ) { return FALSE; }
    // check whether post matches term belongin to tax
    $return = is_object_in_term( $_post->ID, $tax, $term );
    // if error returned, then return false
    if ( is_wp_error( $return ) ) { return FALSE; }
    return $return;
}
