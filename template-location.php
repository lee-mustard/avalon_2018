<?php
/**
 * Template Name: Location Page
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/parts/content-location', 'page'); ?>
<?php endwhile; ?>
